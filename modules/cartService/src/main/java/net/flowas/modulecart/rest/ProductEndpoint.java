package net.flowas.modulecart.rest;

import java.io.InputStream;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import net.flowas.modulecart.domain.Product;

/**
 * 
 */
public class ProductEndpoint
{
   private EntityManager em= Persistence.createEntityManagerFactory ("cartPU").createEntityManager();

   @POST
   @Consumes("application/json")
   public Product create(Product entity)
   {
      em.persist(entity);
      return entity;
   }

   //@Produces("application/octet-stream")
   @GET
   @Path("/images/{id:[0-9][0-9]*}.jpg")
   @Produces("image/jpeg")   
   public byte[] images(@PathParam("id") Long id) throws Exception{
	   System.out.println(id);
	   InputStream fi=Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/resources/"+id+".jpg");
	   byte[] b=new byte[fi.available()];
	   fi.read(b);
	   fi.close();
	   return b;
   }   
   
   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      Product entity = em.find(Product.class, id);
      if (entity == null)
      {
         return Response.status(Status.NOT_FOUND).build();
      }
      em.remove(entity);
      return Response.noContent().build();
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public Response findById(@PathParam("id") Long id)
   {
      TypedQuery<Product> findByIdQuery = em.createQuery("SELECT DISTINCT c FROM Product c LEFT JOIN FETCH c.parent LEFT JOIN FETCH c.children WHERE c.id = :entityId ORDER BY c.id", Product.class);
      findByIdQuery.setParameter("entityId", id);
      Product entity;
      try
      {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre)
      {
         entity = null;
      }
      if (entity == null)
      {
         return Response.status(Status.NOT_FOUND).build();
      }
      return Response.ok(entity).build();
   }

   @GET
   @Path("list.json")
   @Produces("application/json")
   public List<Product> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult)
   {
      TypedQuery<Product> findAllQuery = em.createQuery("SELECT DISTINCT c FROM Product c ORDER BY c.code", Product.class);
      if (startPosition != null)
      {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null)
      {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<Product> results = findAllQuery.getResultList();
      return results;
   }

   @GET
   @Path("last.json")
   @Produces("application/json")
   public List<Product> listLast(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult)
   {
      TypedQuery<Product> findAllQuery = em.createQuery("SELECT DISTINCT c FROM Product c ORDER BY c.code", Product.class);
      if (startPosition != null)
      {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null)
      {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<Product> results = findAllQuery.getResultList();
      return results;
   }
   
   @GET
   @Path("promos.json")
   @Produces("application/json")
   public List<Product> listPromos(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult)
   {
      TypedQuery<Product> findAllQuery = em.createQuery("SELECT DISTINCT c FROM Product c ORDER BY c.code", Product.class);
      if (startPosition != null)
      {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null)
      {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<Product> results = findAllQuery.getResultList();
      return results;
   }
   
   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Consumes("application/json")
   public Response update(Product entity)
   {
      entity = em.merge(entity);
      return Response.noContent().build();
   }
}